## ['id', 'station name', 'address', 'City', 'Status', 'type', 'geo', 'Bikes', 'Places available', 'connection', 'last update', 'localisation']
## https://opendata.lillemetropole.fr/explore/dataset/vlille-realtime/table/
## https://spark.apache.org/docs/latest/api/python/getting_started/quickstart_df.html
## https://medium.com/@jootorres_11979/how-to-set-up-a-hadoop-3-2-1-multi-node-cluster-on-ubuntu-18-04-2-nodes-567ca44a3b12

from pyspark.sql import SparkSession
import pandas as pd
from pyspark.conf import SparkConf

conf = SparkConf()
conf.setMaster("spark://carole-ubuntu:7077").setAppName("vLille")
spark = SparkSession.builder.config(conf=conf).getOrCreate()

vlille_df = pd.read_csv(
    "/home/carole/Documents/spark/vlille-realtime.csv", delimiter=";"
)
df = spark.createDataFrame(vlille_df)

df.select(["station name", "Places available"]).where(df["Places available"] > 0).show()
